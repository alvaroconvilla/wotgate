/*
 *  Copyright (c) 2017, Javier Martínez Villacampa
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package info.ciclope.wotgate.thing.component;

public class ThingDescriptionTag {
    public static final String THING_DESCRIPTION_TYPE = "type";
    public static final String THING_DESCRIPTION_NAME = "name";
    public static final String THING_DESCRIPTION_BASE = "base";
    public static final String THING_DESCRIPTION_GATEKEEPER = "gatekeeper";
    public static final String THING_DESCRIPTION_INTERACTIONS = "interactions";
    public static final String THING_DESCRIPTION_INTERACTION_NAME = "name";
    public static final String THING_DESCRIPTION_INTERACTION_TYPE = "type";
    public static final String THING_DESCRIPTION_INTERACTION_TYPE_PROPERTY = "Property";
    public static final String THING_DESCRIPTION_INTERACTION_TYPE_ACTION = "Action";
    public static final String THING_DESCRIPTION_INTERACTION_WRITABLE = "writable";
    public static final String THING_DESCRIPTION_INTERACTION_ASYNCHRONOUS = "asynchronous";
    public static final String THING_DESCRIPTION_INTERACTION_RESERVABLE = "reservable";
    public static final String THING_DESCRIPTION_INTERACTION_OUTPUTDATA = "outputData";
    public static final String THING_DESCRIPTION_INTERACTION_INPUTDATA = "inputData";
    public static final String THING_DESCRIPTION_INTERACTION_DATA_TYPE = "type";
    public static final String THING_DESCRIPTION_INTERACTION_DATA_TYPE_ARRAY = "array";
    public static final String THING_DESCRIPTION_INTERACTION_DATA_TYPE_OBJECT = "object";
    public static final String THING_DESCRIPTION_INTERACTION_ROLE_BASED_ACCESS_CONTROL = "roleBasedAccessControl";
    public static final String THING_DESCRIPTION_INTERACTION_ROLE_BASED_WRITING_ACCESS_CONTROL = "roleBasedWritingAccessControl";
    public static final String THING_DESCRIPTION_INTERACTION_TASK_STATUS = "status";

    private ThingDescriptionTag() {
    }

}
